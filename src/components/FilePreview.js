import PropTypes from 'prop-types';
import React, {Component} from 'react';

class FilePreview extends Component {
  constructor(props){
    super(props);
    this.state = {
      url_preview: ''
    };
  }

  render() {
    return (
      <h1>Привет, {this.props.name}</h1>
    );
  }
}

FilePreview.propTypes = {
  url: PropTypes.string,
  onChange: PropTypes.func
};

export default FilePreview;
