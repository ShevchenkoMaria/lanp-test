import React, {Component} from 'react';
import PropTypes from "prop-types";

const calculateProgress = (progress) => {
  return progress * 180 / 100
}

const available_file_type = ['image/png', 'image/jpeg']

const toBase64 = file => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);
});

class UploadFile extends Component {
  constructor(props) {
    super(props);
    this.uploading = null;
    this.state = {
      progress: 0,
      is_loading: false,
      is_active_droparea: false,
      error_text: ''
    };
    this.onChangeInput = this.onChangeInput.bind(this);
    this.dropHandler = this.dropHandler.bind(this);
    this.dropAreaHighlight = this.dropAreaHighlight.bind(this);
    this.dropAreaUnhighlight = this.dropAreaUnhighlight.bind(this);
    this.onCancelUpload = this.onCancelUpload.bind(this);
  }

  uploadPhoto() {
    this.setState({is_loading: true});
    return new Promise((resolve) => {
      this.setState({progress: 0}, () => {
        this.uploading = setInterval(() => {
          if (this.state.progress < 100) {
            this.setState({progress: this.state.progress + 5});
          } else {
            clearInterval(this.uploading);
            this.setState({is_loading: false, progress: 0});
            resolve('success');
          }
        }, 100)
      });
    });
  }

  onChangeInput(e) {
    let input = e.target;
    if (input.files && input.files[0]) {
      this.onLoadFile(input.files[0]);
    }
  }

  checkValidationFile(file){
    return new Promise((resolve, reject) => {
      if(!available_file_type.includes(file.type)){
        reject('Unavailable type');
      }
      let _URL = window.URL || window.webkitURL;
      let img = new Image();
      img.onload = function () {
        if(this.width > 100 || this.height > 100){
          reject('Big size');
        }
        else{
          resolve();
        }
      };
      img.src = _URL.createObjectURL(file);
    })
  }

  onLoadFile(file) {
    this.setState({error_text: ''});
    this.checkValidationFile(file)
      .then(() => this.uploadPhoto())
      .then(() => toBase64(file))
      .then((e) => this.props.onChange(e))
      .catch((e) => this.setState({error_text: e}))
  }

  dropHandler(e) {
    this.dropAreaUnhighlight(e);
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      this.onLoadFile(e.dataTransfer.files[0])
    }
    return false;
  }

  dropAreaHighlight(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({is_active_droparea: true})
  }

  dropAreaUnhighlight(e) {
    e.stopPropagation();
    e.preventDefault();
    this.setState({is_active_droparea: false})
  }
  onCancelUpload(){
    clearInterval(this.uploading);
    this.setState({is_loading: false, progress: 0})
  }

  renderPreview() {
    return <div className='upload__file-block block_position_center'>
      <img alt='logo' className='upload__image' src={this.props.value}/>
    </div>
  }

  renderLoading() {
    return <div className='progress__border block_position_center block_relative'>
      <div className="progress__bar" style={{transform: 'rotate(' + calculateProgress(this.state.progress) + 'deg)'}}/>
      <div className="progress__content block_absolute">
        <div className='block_relative block_position_center'>
          {this.renderDefaultImage()}
        </div>
      </div>
    </div>
  }

  renderDefaultImage() {
    return <svg className='block_position_center' width="32" height="48" viewBox="0 0 32 48" fill="none"
                xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd"
            d="M28.072 1.87489C29.3858 1.3494 30.8148 2.31692 30.8148 3.73184V46.7037H1.18518V13.9837C1.18518 13.1659 1.68308 12.4305 2.4424 12.1267L28.072 1.87489Z"
            stroke="#D1E3F8"/>
      <rect x="7.11108" y="17.0745" width="7.40741" height="8.88889" fill="#D1E3F8"/>
      <rect x="17.4814" y="17.0745" width="7.40741" height="2.96296" fill="#D1E3F8"/>
      <rect x="17.4814" y="11.1486" width="7.40741" height="2.96296" fill="#D1E3F8"/>
      <path
        d="M7.11108 29.9264C7.11108 29.3741 7.5588 28.9264 8.11108 28.9264H23.8889C24.4411 28.9264 24.8889 29.3741 24.8889 29.9264V46.7041H7.11108V29.9264Z"
        fill="#D1E3F8"/>
      <rect x="17.4814" y="23.0004" width="7.40741" height="2.96296" fill="#D1E3F8"/>
    </svg>
  }

  renderDefaultBlock() {
    return <div className='upload__file-block block_position_center'>{this.renderDefaultImage()}</div>
  }

  render() {
    return (
      <div className={`upload ${this.state.is_active_droparea && 'upload_highlight'}`}
           onDragOver={this.dropAreaHighlight}
           onDragLeave={this.dropAreaUnhighlight}
           onDrop={this.dropHandler}
           onDragEnter={this.dropAreaHighlight}
      >
        <div className='upload__content block_position_center block__text_center'>
          {
            this.state.is_loading
              ? this.renderLoading()
              : this.props.value
              ? this.renderPreview()
              : this.renderDefaultBlock()
          }
          <div className='block__text block__text_info'>
            {
              this.state.is_loading
                ? 'Uploading'
                : this.props.value
                ? 'Drag & drop here to replace'
                : 'Drag & drop here'
            }
          </div>
          <div className='block__text block__text_secondary'>- or -</div>
          {
            this.state.is_loading
              ? <div onClick={this.onCancelUpload} className='block__text block__text_clickable'>Cancel</div>
              : <label className='block__text block__text_clickable'>
                {
                  this.props.value ? 'Select file to replace' : 'Select file to upload'
                }
                <input onChange={this.onChangeInput} className='block_hidden' type='file'
                       accept={available_file_type.join(',')}/>
              </label>
          }
          <div className='block__text block__text_error'>{this.state.error_text}</div>
        </div>
      </div>
    );
  }
}

UploadFile.propTypes = {
  value: function(props, propName, componentName) {
    var base64regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}))/;
    if (props[propName] && !base64regex.test(props[propName])) {
      return new Error(
        'Некорректное свойство `' + propName + '` передано в компонент' +
        ' `' + componentName + '`. Валидация прошла неудачно.'
      );
    }
  },
  onChange: PropTypes.func
};

export default UploadFile;
