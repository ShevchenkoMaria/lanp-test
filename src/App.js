import React, {Component} from 'react';
import './App.css';
import './assets/style.scss';
import UploadFile from "./components/UploadFile";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
    };
    this.onChangeFile = this.onChangeFile.bind(this);
  }

  onChangeFile(new_url){
    this.setState({url: new_url})
  }
  render() {
    return (
      <div className='block block_position_center'>
        <div className='block__header'>
          <div className='block__title'>Company logo</div>
          <div className='block__text block__text_secondary'>Logo should be square, 100px size and in png, jpeg file
            format.
          </div>
        </div>
        <div className='block__body'>
          <UploadFile value={this.state.url} onChange={this.onChangeFile}/>
        </div>
      </div>
    );
  }
}

export default App;
